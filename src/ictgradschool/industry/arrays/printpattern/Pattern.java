package ictgradschool.industry.arrays.printpattern;

public class Pattern {
    int numberOfCharacters;
    char symbol;
    public Pattern(int numberOfCharacters, char symbol) {
        this.numberOfCharacters=numberOfCharacters;
        this.symbol=symbol;
    }

    public void setNumberOfCharacters(int numberOfCharacters) {
        this.numberOfCharacters = numberOfCharacters;
    }

    public int getNumberOfCharacters() {

        return numberOfCharacters;
    }

    @Override
    public String toString() {
        String pattern="";
        for (int i  = 0; i <numberOfCharacters ; i++) {
            pattern=pattern+symbol;
        }
        return pattern;
    }
}
