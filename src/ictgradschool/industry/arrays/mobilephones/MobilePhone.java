package ictgradschool.industry.arrays.mobilephones;

import java.util.Objects;

public class MobilePhone {

    // TODO Declare the 3 instance variables:
    String brand;
    String model;
    double price;
    
    public MobilePhone(String brand, String model, double price) {
        // Complete this constructor method
        this.brand=brand;
        this.model=model;
        this.price=price;
    }

    // TODO Uncomment these methods once the corresponding instance variable has been declared.
    public String getBrand() {
        return brand;
    }
    
    public void setBrand(String brand) {
        this.brand = brand;
    }
    
    // TODO Insert getModel() method here

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    // TODO Insert setModel() method here
    
    // TODO Insert getPrice() method here

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    // TODO Insert setPrice() method here
    
    // TODO Insert toString() method here

    @Override
    public String toString() {
        return brand+ " " +model+" which cost $"+price;
    }

    // TODO Insert isCheaperThan() method here
    public boolean isCheaperThan(MobilePhone other){
        return this.price<other.getPrice();
    }
    // TODO Insert equals() method here

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof MobilePhone)) return false;
        MobilePhone that = (MobilePhone) o;
        return Double.compare(that.price, price) == 0 &&
                Objects.equals(brand, that.brand) &&
                Objects.equals(model, that.model);
    }


}


